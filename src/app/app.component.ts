import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  //Primjer podataka dohvaćenih od API-a
  userComments: string[] = [
    'Veoma dobri savjeti!',
    'Odlično odrađeno!',
    "Super!<img src='data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=' width='0' height='0' onload='console.log(document.cookie);'/>",
  ];

  constructor() {
    //Dodavanje kolačića u aplijaciju radi demonstracije Angular-ove sigurnosti
    document.cookie =
      'user=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImlzbW9rQHZ1Yi5ociIsIm5hbWUiOiJJdmFuIFNtb2siLCJpYXQiOjE1MTYyMzkwMjJ9.wqOIuAqsZUC9NoHnQt9QPzbDh3TeejhbstpxtyvekPw';
  }
}
